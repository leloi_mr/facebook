# coding=utf-8
import shutil

from app import *

# import base64
# import paramiko
#
# def get_list_ssh(ssh_file):
#     try:
#         res = []
#         with open(ssh_file, 'r') as f_ssh:
#             ssh_properties = f_ssh.readlines()
#         for line in ssh_properties:
#             ssh = line.split('|')
#             res.append({
#                 'ip': ssh[0],
#                 'port': 22,
#                 'user': ssh[1],
#                 'passwd': ssh[2],
#                 'country': ssh[3]
#             })
#         return res
#     except Exception as ex:
#         write_log(ex)
#
# from sshtunnel import SSHTunnelForwarder
#
# def connect_ssh_tunnel(ssh, proxy_addr, proxy_port):
#     # client = False
#     server = False
#     ssh_addr = ssh['ip'] +':'+ str(ssh['port'])
#     # write_log('Connecting ssh: '+ ssh_addr)
#
#     try:
#         # client = paramiko.SSHClient()
#         # key = paramiko.RSAKey(data=base64.b64decode(b'123456'))
#         # client.get_host_keys().add(ssh_addr, 'ssh-rsa', key)
#
#         # client.load_system_host_keys()
#         # client.set_missing_host_key_policy(paramiko.WarningPolicy())
#         #
#         # client.connect(ssh['ip'], ssh['port'], username=ssh['user'], password=ssh['passwd'])
#         # write_log('Connect success')
#         server = SSHTunnelForwarder(
#             ssh_addr,
#             ssh_username=ssh['user'],
#             ssh_password=ssh['passwd'],
#             remote_bind_address=(proxy_addr, proxy_port)
#         )
#
#         server.start()
#         return server  # show assigned local port
#         # work with `SECRET SERVICE` through `server.local_bind_port`.
#
#         # server.stop()
#     except Exception as ex:
#         return False

executable_path = 'libs/webdriver/chromedriver.exe'

def copytree(src, dst, symlinks=False, ignore=None):
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)

source_user_profile = 'chromium-profile/default'

def open_webdriver(user_name, proxy_port):
    path_profile = 'chromium-profile/{0}'.format(user_name)
    if not os.path.exists(path_profile):
        copytree(source_user_profile, path_profile)

        if not os.path.isdir(path_profile):
            write_log('Cannot copy folder profile: {0}'.format(path_profile))
            exit(0)
        else:
            write_log('Created folder profile: {0}'.format(path_profile))
    write_log('Connecting user: {0} - port: {1}'.format(user_name, proxy_port))

    # server_ssh = connect_ssh_tunnel(ssh, proxy_addr, proxy_port)
    server_ssh = True
    browser = True
    if server_ssh:
        try:
            options = webdriver.ChromeOptions()
            is_headless = False
            if is_headless:
                options.add_argument('headless')
            options.add_argument('--no-sandbox')
            options.add_argument('--no-default-browser-check')
            options.add_argument('--disable-gpu')
            options.add_argument("--disable-popup-blocking")
            # options.add_argument('--disable-extensions')
            options.add_argument('--disable-default-apps')
            options.add_argument('--disable-infobars')
            options.add_argument('--disable-notifications')
            options.add_argument('--disable-bundled-ppapi-flash')
            options.add_argument('--disable-plugins-discovery')
            options.add_argument('--disable-session-crashed-bubble')

            if proxy_port:
                socks5 = 'socks5://{0}:{1}'.format(proxy_addr, proxy_port)
                options.add_argument('--proxy-server={0}'.format(socks5))
                # fix_dns_rules = 'MAP * 0.0.0.0 , EXCLUDE {0}'.format(proxy_addr)
                # options.add_argument("--host-resolver-rules={0}".format(fix_dns_rules))
            options.add_argument('user-data-dir=chromium-profile/{0}'.format(user_name))
            # options.add_argument('--profile-directory={0}'.format(user_name))
            desired_capabilities = webdriver.DesiredCapabilities.CHROME.copy()
            desired_capabilities['chrome.page.customHeaders.User-Agent'] = get_headers()
            desired_capabilities['chrome.page.customHeaders.X-Frame-Options'] = 'sameorigin'
            log_chromium_path = 'chromium-log/{0}.log'.format(user_name)
            service_args = ['--verbose', '--log-path={0}'.format(log_chromium_path)]

            browser = webdriver.Chrome(
                executable_path=executable_path,
                chrome_options=options,
                service_args=service_args,
                desired_capabilities=desired_capabilities)

            url = 'https://whatismyipaddress.com/blacklist-check'
            browser.get(url)

            # browser.quit()
            # break
        except Exception as ex:
            browser = False
            write_log(ex)
    else:
        write_log('ERROR ssh. Next ...')

# Set-up cookie
# browser.get('http://google.com/favicon.ico')
# cookie_file = 'gck.txt'
# cookie = get_cookie_jar(cookie_file, '.google.com')
# for ck in cookie:
#     browser.add_cookie(ck)

# Set-up ssh
# ssh_file = 'ssh-06-10-18.txt'
# list_ssh = get_list_ssh(ssh_file)
#print('List size: {0}'.format(len(list_ssh)))

proxy_addr = '127.0.0.1'
list_socks_port = (62411,)
list_users = ('user4',)

from multiprocessing import Process, freeze_support

if __name__ == '__main__':
    freeze_support()

    processes = []
    for user_name, proxy_port in zip(list_users, list_socks_port):
        p = Process(target=open_webdriver, args=(user_name, proxy_port))
        processes.append(p)
        p.start()
    for p in processes:
        p.join()
