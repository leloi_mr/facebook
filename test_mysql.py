import mysql.connector as mysqldb
import time

conn = mysqldb.connect(
    user='root',
    password='',
    database='fb')
cursor = conn.cursor()

sql = "INSERT INTO users_mem (name) VALUES (%s);"
data = [("le",)]*10000 # no-index 0.292000055313
# index 0.298000097275

sql = "SELECT name FROM users_mem WHERE name = 'le';" # 0.00300002098083
#sql = "SELECT name FROM users WHERE MATCH(name) AGAINST('loi');" # 0.000999927520752

#sql = "UPDATE users SET name = 'le' WHERE name = 'loi';" # 0.163000106812
#sql = "DELETE FROM users_mem WHERE name = 'le';" # 0.219000101089

#data = ("loi",)
t1 = time.time()
#cursor.executemany(sql, data)
cursor.execute(sql)
#for i in range(10000):
#    cursor.execute(sql, data)
#    conn.commit() # 3.5501
res = cursor.fetchall()
t2 = time.time()
print(t2 - t1)
