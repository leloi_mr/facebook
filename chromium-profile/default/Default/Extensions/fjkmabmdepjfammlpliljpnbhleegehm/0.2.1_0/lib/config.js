var config = {};

config.welcome = {
  get version () {return app.storage.read("version")},
  set version (val) {app.storage.write("version", val)}
};

config.addon = {
  set state (val) {app.storage.write("state", val)},
  set inject (val) {app.storage.write("inject", val)},
  set webrtc (val) {app.storage.write("webrtc", val)},
  get inject () {return app.storage.read("inject") !== undefined ? app.storage.read("inject") : true},
  get state () {return app.storage.read("state") !== undefined ? app.storage.read("state") : "enabled"},
  get webrtc () {return app.storage.read("webrtc") !== undefined ? app.storage.read("webrtc") : "disable_non_proxied_udp"}
};
