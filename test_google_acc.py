# coding=utf-8
from selenium.webdriver.common.keys import Keys

from app import *

# Reset new ip
# reset_dcom()

is_headless = False

# Custom headless option
options = webdriver.ChromeOptions()
if is_headless:
    options.add_argument('headless')
options.add_argument('--no-sandbox')
options.add_argument('--no-default-browser-check')
options.add_argument('--disable-gpu')
options.add_argument('--disable-extensions')
options.add_argument('--disable-default-apps')

desired_capabilities = webdriver.DesiredCapabilities.CHROME.copy()
desired_capabilities['chrome.page.customHeaders.User-Agent'] = get_headers()

browser = webdriver.Chrome(
    executable_path='libs/webdriver/chromedriver.exe',
    chrome_options=options,
    desired_capabilities=desired_capabilities)

# Set-up cookie
# browser.get('http://google.com/favicon.ico')
# cookie_file = 'test_google_acc_ck.txt'
# cookie = get_cookie_jar(cookie_file, '.google.com')
# for ck in cookie:
#     browser.add_cookie(ck)

try:
    url = 'https://www.google.com/'
    browser.get(url)
    txtarea = browser.find_element_by_css_selector('input[name=q]')
    txtarea.clear()
    txtarea.send_keys('create google account')
    time.sleep(2)
    txtarea.send_keys(Keys.ENTER)
    time.sleep(5)

    url = 'https://accounts.google.com/SignUp?hl=vi'
    browser.get(url)
    wait_selector = 'div#accountDetailsNext'
    time_wait_load_page = 10
    browser_wait(browser, time_wait_load_page, wait_selector)
    time.sleep(2)

    # js = open('js_test_google_acc.txt', 'r').read()
    # cmt = lorem.sentence()
    # js = js.replace('changetext', cmt)
    # browser.execute_script(js)

    txtarea = browser.find_element_by_css_selector('input[name=lastName]')
    txtarea.clear()
    txtarea.send_keys('Loi')
    time.sleep(2)
    txtarea = browser.find_element_by_css_selector('input[name=firstName]')
    txtarea.clear()
    txtarea.send_keys('Le')
    time.sleep(2)
    txtarea = browser.find_element_by_css_selector('input[name=Username]')
    txtarea.clear()
    txtarea.send_keys('leloikv4')
    time.sleep(4)
    txtarea = browser.find_element_by_css_selector('input[name=Passwd]')
    txtarea.clear()
    txtarea.send_keys('123456@google')
    time.sleep(2)
    txtarea = browser.find_element_by_css_selector('input[name=ConfirmPasswd]')
    txtarea.clear()
    txtarea.send_keys('123456@google')
    time.sleep(4)

    submit = browser.find_element_by_css_selector('div#accountDetailsNext')
    submit.click()

    # browser_wait(browser, time_wait_load_page, wait_selector)
    # print(url)
except Exception as ex:
    browser.quit()
    write_log(ex)
