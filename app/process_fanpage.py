import time

import lorem
from selenium.common.exceptions import TimeoutException
from utils import *

from app import browser_wait, write_log

def post_to_page(browser, id_page, time_wait_load_page):
    browser.get('https://m.facebook.com/'+ str(id_page))
    post = lorem.sentence()
    wait_selector = 'div[data-sigil$=page_post_button]'

    try:
        browser_wait(browser, time_wait_load_page, wait_selector)

        inp_selector = wait_selector
        inp = browser.find_element_by_css_selector(inp_selector)
        inp.click()
        
        txtarea = browser.find_element_by_css_selector('textarea.composerInput')
        txtarea.clear()
        txtarea.send_keys(post)
        
        btn = browser.find_element_by_css_selector('button[data-sigil$=submit_composer]')
        btn.click()
    except TimeoutException as ex:
        write_log("Timed out waiting for page to load")
        write_log(ex)

def get_list_post_from_page(browser, id_page, time_wait_load_page):
    browser.get('https://m.facebook.com/'+ str(id_page))
    wait_selector = '#pages_msite_body_contents'
    
    try:
        browser_wait(browser, time_wait_load_page, wait_selector)
        
        js = open(get_scripts_js_file('fanpage/js_scroll_fanpage.txt'), 'r').read()
        js = js.replace('changetimeintervalscroll', '2000')
        js = js.replace('changetimeforcestopscroll', '10000')
        browser.execute_script(js)
        time.sleep(10)

        js = open(get_scripts_js_file('fanpage/js_get_list_posts_fanpage.txt'), 'r').read()
        browser.execute_script(js)
        time.sleep(2)
        
        html = browser.find_elements_by_css_selector('body')[0]
        res = html.text.encode('utf-8')
        
        write_file(get_fanpage_list_posts_file(id_page), res)
    except Exception as ex:
        write_log(ex)

def process_list_post_from_page(browser, id_page, time_wait_load_page):
    file = get_fanpage_list_posts_file(id_page)
    list = []
    with open(file, 'r') as f:
        list = f.read().split(':::')
        for url in list:
            process_comment_to_a_post_fanpage(browser, url, time_wait_load_page)
            break

def process_comment_to_a_post_fanpage(browser, url, time_wait_load_page):
    browser.get(url)
    wait_selector = 'button[name="submit"]'

    try:
        browser_wait(browser, time_wait_load_page, wait_selector)

        js = open(get_scripts_js_file('fanpage/js_comment_to_a_post_fanpage.txt'), 'r').read()
        cmt = lorem.sentence()
        js = js.replace('changetext', cmt)
        browser.execute_script(js)

        browser_wait(browser, time_wait_load_page, wait_selector)
        # print(url)
    except Exception as ex:
        write_log(ex)