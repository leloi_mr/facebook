def post_to_group(browser, id_group, time_wait_load_group):
    browser.get('https://m.facebook.com/'+ str(id_group))
    post = lorem.sentence()
    wait_selector = 'button[data-sigil^=show_composer]'

    try:
        browser_wait(browser, time_wait_load_group, wait_selector)

        inp_selector = wait_selector
        inp = browser.find_element_by_css_selector(inp_selector)
        inp.click()
        
        txtarea = browser.find_element_by_css_selector('textarea.composerInput')
        txtarea.clear()
        txtarea.send_keys(post)
        
        btn = browser.find_element_by_css_selector('button[data-sigil$=submit_composer]')
        btn.click()
    except TimeoutException as ex:
        write_log("Timed out waiting for page to load")
        write_log(ex)

def get_list_post_from_group(browser, id_group, time_wait_load_group):
    browser.get('https://m.facebook.com/'+ str(id_group))
    wait_selector = '.story_body_container'
    
    try:
        browser_wait(browser, time_wait_load_group, wait_selector)
        
        js = open('js_scroll_group.txt', 'r').read()
        js = js.replace('changetimeintervalscroll', '2000')
        js = js.replace('changetimeforcestopscroll', '10000')
        browser.execute_script(js)
        time.sleep(10)

        js = open('js_get_list_posts_group.txt', 'r').read()
        browser.execute_script(js)
        time.sleep(2)
        
        html = browser.find_elements_by_css_selector('body')[0]
        res = html.text.encode('utf-8')
        
        with open('list_posts_group_'+ str(id_group) +'.txt', 'w') as f:
            f.write(res)
    except Exception as ex:
        write_log(ex)

def process_list_post_from_group(browser, id_group, time_wait_load_group):
    file = 'list_posts_group_'+ str(id_group) +'.txt'
    list = []
    with open(file, 'r') as f:
        list = f.read().split(':::')
        for url in list:
            browser.get(url)
            wait_selector = 'button[name="submit"]'

            try:
                browser_wait(browser, time_wait_load_group, wait_selector)
                
                js = open('js_comment_to_a_post_group.txt', 'r').read()
                cmt = lorem.sentence()
                js = js.replace('changetext', cmt)
                browser.execute_script(js)

                browser_wait(browser, time_wait_load_group, wait_selector)
                print(url)
                #time.sleep(2)

                break
            except Exception as ex:
                write_log(ex)
