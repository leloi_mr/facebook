#!/usr/bin/python
from flask import Flask
from flask import request

app = Flask(__name__)

import time
from selenium.common.exceptions import TimeoutException
from utils import *

executable_path = "/usr/bin/chromedriver"
options = webdriver.ChromeOptions()

options.add_argument('--headless')
options.add_argument('--no-sandbox')
options.add_argument('--no-default-browser-check')
options.add_argument('--disable-gpu')
options.add_argument('--disable-dev-shm-usage')
options.add_argument("--disable-popup-blocking")
options.add_argument('--disable-extensions')
options.add_argument('--disable-default-apps')
options.add_argument('--disable-infobars')
options.add_argument('--disable-notifications')
options.add_argument('--disable-bundled-ppapi-flash')
options.add_argument('--disable-plugins-discovery')
options.add_argument('--disable-session-crashed-bubble')

browser = webdriver.Chrome(
executable_path=executable_path,
chrome_options=options)

@app.route('/render/<path:url>')
def render(url):
    browser.get('%s' % url)
    html = browser.page_source

    return html

@app.route('/reg')
def reg_mobile():
    html = None
    browser.get('https://m.facebook.com/reg-no-deeplink/?cid=103')

    wait_selector = 'button[data-sigil="touchable multi_step_next"]'
    time_wait = 5

    try:
        browser_wait(browser, time_wait, wait_selector)

        txt = browser.find_element_by_css_selector('input[name=lastname]')
        txt.clear()
        txt.send_keys('Adam')

        txt = browser.find_element_by_css_selector('input[name=firstname]')
        txt.clear()
        txt.send_keys('Kyle')

        btn = browser.find_element_by_css_selector(wait_selector)
        btn.click()

        # step2
        wait_selector = 'button[data-sigil="touchable multi_step_next"]'
        time_wait = 5
        browser_wait(browser, time_wait, wait_selector)

        select = Select(browser.find_element_by_css_selector('select[name=birthday_day]'))
        select.select_by_value('8')

        select = Select(browser.find_element_by_css_selector('select[name=birthday_month]'))
        select.select_by_value('8')

        select = Select(browser.find_element_by_css_selector('select[name=birthday_year]'))
        select.select_by_value('1995')

        btn = browser.find_element_by_css_selector(wait_selector)
        btn.click()

        # step3
        wait_selector = 'a[data-sigil="switch_phone_to_email"]'
        time_wait = 5
        browser_wait(browser, time_wait, wait_selector)

        btn = browser.find_element_by_css_selector(wait_selector)
        btn.click()

        # step4
        wait_selector = 'button[data-sigil="touchable multi_step_next"]'
        time_wait = 5
        browser_wait(browser, time_wait, wait_selector)

        html = '<form method="POST" action="./regStep2"><input type="email" name="mail" value=""><button type="submit">OK</button></form>'
    except Exception as ex:
	html = browser.page_source
        write_log("Timed out step1")
        write_log(ex)

    return html

@app.route('/regStep2', methods=['POST', 'GET'])
def reg_mobile_step2():
    html = None

    wait_selector = 'button[data-sigil="touchable multi_step_next"]'
    time_wait = 5

    try:
        browser_wait(browser, time_wait, wait_selector)

        if request.method == 'POST':
            if request.form['mail']:
					mail = request.form['mail']

					txt = browser.find_element_by_css_selector('input[name=reg_email__]')
					txt.clear()
					txt.send_keys(mail)

					btn = browser.find_element_by_css_selector(wait_selector)
					btn.click()

					# step5
					wait_selector = 'button[data-sigil="touchable multi_step_next"]'
					time_wait = 5
					browser_wait(browser, time_wait, wait_selector)

					browser.find_element_by_css_selector('input[name="sex"][value="2"]').click()

					btn = browser.find_element_by_css_selector(wait_selector)
					btn.click()

					# step6
					wait_selector = 'button[data-sigil="touchable multi_step_submit"]'
					time_wait = 5
					browser_wait(browser, time_wait, wait_selector)

					txt = browser.find_element_by_css_selector('input[name="reg_passwd__"]')
					txt.clear()
					txt.send_keys('Kyle123456')

					btn = browser.find_element_by_css_selector(wait_selector)
					btn.click()

					time.sleep(5)
					#browser_wait(browser, time_wait, wait_selector)
					#browser.execute_script("return document.documentElement.outerHTML;")
					html = browser.page_source

    except Exception as ex:
        html = browser.page_source
        write_log("Timed out step2")
        write_log(ex)

    return html

if __name__ == '__main__':
        app.run(host="0.0.0.0", port=5000)

